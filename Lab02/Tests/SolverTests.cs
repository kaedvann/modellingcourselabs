﻿using System.Linq;
using Lab02;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class SolverTests
    {
        [Test]
        public void TestMatrixCreation()
        {
            var matr = new double[,] {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
            var solver = new EquationSolver(matr);
            Assert.AreEqual(-5.0, solver.EquationMatrix[0,0]);
            Assert.AreEqual(4.0, solver.EquationMatrix[0,1]);
            Assert.AreEqual(7.0, solver.EquationMatrix[0,2]);
        }

        [Test]
        public void TestSolving()
        {
            var matr = new double[,] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
            var solver = new EquationSolver(matr);
            var result = solver.Solve();
            Assert.IsTrue(result.All(e => e < 1.0));
            Assert.AreEqual(1.0, result.Sum(), 1e-4);
        }
    }
}
