﻿using System;
using System.Windows.Forms;

namespace Lab02
{
    public partial class MainWindow : Form
    {
        private int _dimensions;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnSetDimensions_Click(object sender, System.EventArgs e)
        {
            try
            {
                var dim = int.Parse(TbDimensions.Text);
                if (dim < 0 || dim > 100)
                    throw new ArgumentException();
                _dimensions = dim;
                InitializeGrids();
            }
            catch (Exception)
            {
                MessageBox.Show("Некорректно задано число состояний");
            }
        }

        private void InitializeGrids()
        {
            GridInput.RowCount = GridInput.ColumnCount = GridResult.ColumnCount  = _dimensions;
            GridResult.RowCount = 1;
            for (int i = 0; i < _dimensions; ++i)
            {
                GridInput.Rows[i].HeaderCell.Value = "S" + i;
                GridInput.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
                for (int j = 0; j < _dimensions; ++j)
                {
                    if (i == 0)
                        GridResult.Rows[i].Cells[j].Value = 0;
                    GridInput.Rows[i].Cells[j].Value = 0;
                    GridInput.Columns[j].HeaderCell.Value = "S" + j;
                    GridResult.Columns[j].HeaderCell.Value = "S" + j;
                }
            }
        }

        private void BtnSolve_Click(object sender, EventArgs e)
        {
            try
            {
                var solver = CreateSolver();
                var result = solver.Solve();
                for (int i = 0; i < _dimensions; ++i)
                    GridResult.Rows[0].Cells[i].Value = result[i].ToString("G4");
            }
            catch (Exception)
            {
                MessageBox.Show("Некорректно заданы значения интенсивностей");
            }
        }

        private EquationSolver CreateSolver()
        {
            var matr = new double[_dimensions, _dimensions];
            for(int i = 0; i < _dimensions; ++i)
                for (int j = 0; j < _dimensions; ++j)
                {
                    matr[i, j] = double.Parse(GridInput.Rows[i].Cells[j].Value.ToString());
                    if (matr[i, j] < 0)
                        throw new ArgumentException();
                }
            return new EquationSolver(matr);
        }
    }
}
