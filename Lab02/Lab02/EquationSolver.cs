﻿using MathNet.Numerics.LinearAlgebra;

namespace Lab02
{
    internal static class MatrixExtension
    {
        public static double RowSumWithoutDiag(this double[,] matrix, int row)
        {
            double sum = 0.0;
            for (int j = 0; j < matrix.GetLength(1); ++j)
                if (row != j)
                    sum += matrix[row, j];
            return sum;
        }
    }

    public class EquationSolver
    {
        private readonly Matrix<double> _equationMatrix; 
        public EquationSolver(double[,] intensivityMatrix)
        {
            _equationMatrix = Matrix<double>.Build.Dense(intensivityMatrix.GetLength(0), intensivityMatrix.GetLength(1));

            for (int i = 0; i < EquationMatrix.RowCount - 1; ++i)
            {
                for (int j = 0; j < EquationMatrix.ColumnCount; ++j)
                {
                    if (i == j)
                        EquationMatrix[i, j] = -intensivityMatrix.RowSumWithoutDiag(i);
                    else
                        EquationMatrix[i, j] = intensivityMatrix[j, i] ;
                }
            }
            for (int j = 0; j < EquationMatrix.ColumnCount; ++j)
                EquationMatrix[EquationMatrix.RowCount - 1, j] = 1;
        }

        public Vector<double> Solve()
        {
            Vector<double> v = Vector<double>.Build.Dense(_equationMatrix.RowCount);
            v[v.Count - 1] = 1;
            return  EquationMatrix.Solve(v);
        }

        public Matrix<double> EquationMatrix
        {
            get { return _equationMatrix; }
        }
    }
}
