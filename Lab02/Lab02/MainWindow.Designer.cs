﻿namespace Lab02
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GridInput = new System.Windows.Forms.DataGridView();
            this.GridResult = new System.Windows.Forms.DataGridView();
            this.BtnSetDimensions = new System.Windows.Forms.Button();
            this.BtnSolve = new System.Windows.Forms.Button();
            this.TbDimensions = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.GridInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridResult)).BeginInit();
            this.SuspendLayout();
            // 
            // GridInput
            // 
            this.GridInput.AllowUserToAddRows = false;
            this.GridInput.AllowUserToDeleteRows = false;
            this.GridInput.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridInput.Location = new System.Drawing.Point(12, 91);
            this.GridInput.Name = "GridInput";
            this.GridInput.Size = new System.Drawing.Size(932, 351);
            this.GridInput.TabIndex = 0;
            // 
            // GridResult
            // 
            this.GridResult.AllowUserToAddRows = false;
            this.GridResult.AllowUserToDeleteRows = false;
            this.GridResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridResult.Location = new System.Drawing.Point(12, 496);
            this.GridResult.Name = "GridResult";
            this.GridResult.Size = new System.Drawing.Size(932, 60);
            this.GridResult.TabIndex = 1;
            // 
            // BtnSetDimensions
            // 
            this.BtnSetDimensions.Location = new System.Drawing.Point(174, 21);
            this.BtnSetDimensions.Name = "BtnSetDimensions";
            this.BtnSetDimensions.Size = new System.Drawing.Size(114, 23);
            this.BtnSetDimensions.TabIndex = 2;
            this.BtnSetDimensions.Text = "Применить";
            this.BtnSetDimensions.UseVisualStyleBackColor = true;
            this.BtnSetDimensions.Click += new System.EventHandler(this.BtnSetDimensions_Click);
            // 
            // BtnSolve
            // 
            this.BtnSolve.Location = new System.Drawing.Point(390, 20);
            this.BtnSolve.Name = "BtnSolve";
            this.BtnSolve.Size = new System.Drawing.Size(96, 23);
            this.BtnSolve.TabIndex = 3;
            this.BtnSolve.Text = "Решить";
            this.BtnSolve.UseVisualStyleBackColor = true;
            this.BtnSolve.Click += new System.EventHandler(this.BtnSolve_Click);
            // 
            // TbDimensions
            // 
            this.TbDimensions.Location = new System.Drawing.Point(33, 23);
            this.TbDimensions.Name = "TbDimensions";
            this.TbDimensions.Size = new System.Drawing.Size(119, 20);
            this.TbDimensions.TabIndex = 4;
            this.TbDimensions.Text = "5";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Количество состояний";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Исходные данные";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 480);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Решение";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(967, 568);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TbDimensions);
            this.Controls.Add(this.BtnSolve);
            this.Controls.Add(this.BtnSetDimensions);
            this.Controls.Add(this.GridResult);
            this.Controls.Add(this.GridInput);
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lab02";
            ((System.ComponentModel.ISupportInitialize)(this.GridInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridResult)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView GridInput;
        private System.Windows.Forms.DataGridView GridResult;
        private System.Windows.Forms.Button BtnSetDimensions;
        private System.Windows.Forms.Button BtnSolve;
        private System.Windows.Forms.TextBox TbDimensions;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

