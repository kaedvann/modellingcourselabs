﻿using System;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Lab3
{
    public partial class ChartForm : Form
    {
        double _a;
        double _b;
        double _xmin;
        double _xmax;
        double _mu;
        double _sigma;
        const double Eps = 0.1e-5;

        public ChartForm()
        {
            InitializeComponent();
            method.SelectedIndex = 0;
        }

        private void method_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (method.SelectedIndex == 1)
            {
                //norm
                labelFirstParam.Text = "µ";
                labelSecondParam.Text = "σ";
            }
            else
            {
                labelFirstParam.Text = "a";
                labelSecondParam.Text = "b";
            }
        }

        static double FuncT(double x)
        {
            return Math.Exp(-x*x);
        }
        double SimpsonIntegr(double a,double b)
        {
            return ((b-a) / 6) * (FuncT(a) + 4*FuncT((a+b)/2) + FuncT(b));
        }
        double Erf(double x)
        {
            double a = 0.0;
            double b = x;
            double tst, tst1, incr;
            int c = 2;
            tst1 = SimpsonIntegr(a,b);
            do {
                  tst = tst1;
                  tst1 = 0;
                  incr = (b - a) / c;
                  for (int i=0;i<c;++i) {
                    tst1 = tst1 + SimpsonIntegr(a + incr * i, a + incr * (i + 1));
                  }
                  c += 1;
            } while (Math.Abs(tst - tst1) >= Eps);
            return 2*tst1/Math.Sqrt(Math.PI);
        }


        public double UniformMean()
        {
            return (_a + _b) / 2.0;
        }

        public double UniformVariance()
        {
            return Math.Pow((_b - _a), 2.0) / 12.0;
        }


        public double UniformDistribution(double x)
        {
            double res = 0;
            if (x >= _b)
                res = 1.0;
            else if (x >= _a)
                res = (x - _a) / (_b - _a);
            return res;
        }

        private double UniformDensity(double x)
        {
            double res = 0;
            if (x >= _a && x <= _b)
                res = 1.0 / (_b - _a);
            return res;
        }

        private double NormalDistribution(double k)
        {
            return (1+Erf((k-_mu)/(Math.Sqrt(2*Math.Pow(_sigma,2)))))/2;
        }


        public double NormalDensity(double k)
        {
            return 1/(_sigma*Math.Sqrt(2*Math.PI))*Math.Exp(-Math.Pow(k - _mu,2)/(2*Math.Pow(_sigma,2)));
        }

        private void FillGraphic(DataPointCollection collection, Func<double, double> func, bool truncate = false)
        {
            collection.Clear();
            double step = method.SelectedIndex == 0 ? 0.1 : 0.5;
            for (double i = _xmin; i < _xmax; i += step)
            {
                if (method.SelectedIndex == 0 && ((i <= _a) || i >=_b) && truncate)
                    continue;
                collection.AddXY(i, func(i));
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(Erf(0).ToString());
            try
            {
                _xmin = double.Parse(textLeftX.Text);
                _xmax = double.Parse(textRightX.Text);
                if (_xmin >= _xmax)
                {
                    MessageBox.Show("Некорректные границы отрезка");
                    return;
                }
                if (method.SelectedIndex == 0)
                {
                    _a = double.Parse(textFirstParam.Text);
                    _b = double.Parse(textSecondParam.Text);
                    FillGraphic(chart.Series["F(x)"].Points, UniformDistribution);
                    FillGraphic(chart.Series["f(x)"].Points, UniformDensity, true);
                    label5.Text = "MX = " + UniformMean();
                    label6.Text = "DX = " + UniformVariance();
                }
                else
                {
                    _mu = double.Parse(textFirstParam.Text);
                    _sigma = double.Parse(textSecondParam.Text);
                    FillGraphic(chart.Series["F(x)"].Points, NormalDistribution);
                    label5.Text = "MX = " + _mu;
                    label6.Text = "DX = " + Math.Pow(_sigma,2);
                    FillGraphic(chart.Series["f(x)"].Points, NormalDensity);
                    
                }
            }
            catch (FormatException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}