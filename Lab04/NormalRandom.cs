using System;

namespace Model4
{
	public class NormalRandom : BaseRandom
	{
		public NormalRandom(double sigma, double m) : base(0)
		{
			this.sigma = sigma;
			this.m = m;
		}

		public NormalRandom(double sigma, double m, int seed) : base(seed)
		{
			this.sigma = sigma;
			this.m = m;
		}

		public override double GenerateNext()
		{
            //double sum = 0;
            //for (int i = 0; i < 12; i++)
            //    sum += GenerateNormalized();
            //return sigma*(sum - 6) + m;
            
            //�������������� �����-�������
		    var r = GenerateNormalized();
		    var phi = GenerateNormalized();
		    return m + sigma*Math.Cos(2*Math.PI*phi)*Math.Sqrt(-2.0*Math.Log(r));
		}

		private double sigma, m;
	}
}
