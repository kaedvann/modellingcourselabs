namespace Model4
{
	public interface IGenerator
	{
		void AddReceiver(IReceiver rcvr);
		double NextGenerationPeriod();
		void SendRequest();
	}
}
