using System;

namespace Model4
{
    public class Controller
    {
        private readonly IGenerator _gen;
        private readonly Processor _pr;
        private readonly int _requests;
        private readonly double _returnProbability;
        private readonly Random _returnRand = new Random();

        private double _genNext;
        private double _prNext;
        private double _time;

        public Controller(IGenerator gen, Processor pr, int requests, double returnProbability = 0.0)
        {
            _gen = gen;
            _pr = pr;
            _requests = requests;
            _returnProbability = returnProbability;
            gen.AddReceiver(pr);
        }

        public void Dispatch()
        {
            _time = 0;
            _gen.SendRequest();
            _genNext = _gen.NextGenerationPeriod();
            _prNext = _pr.NextProcessingPeriod();

            int i = 0;
            do
            {
                if (_genNext <= _prNext)
                {
                    _time = _genNext;
                    if (_pr.GetQueueSize() == 0)
                        _prNext = _time + _pr.NextProcessingPeriod();
                    _gen.SendRequest();
                    i++;
                    _genNext = _time + _gen.NextGenerationPeriod();
                }
                else if (_pr.GetQueueSize() > 0)
                {
                    _time = _prNext;
                    _pr.Process();
                    if (Returns())
                        _gen.SendRequest();
                    if (_pr.GetQueueSize() > 0)
                        _prNext = _time + _pr.NextProcessingPeriod();
                    else
                        _prNext = _genNext;
                }
            }
            while (i < _requests);
        }

        private bool Returns()
        {
            return _returnRand.NextDouble() < _returnProbability;
        }

        public int GetTotalRequests()
        {
            return _pr.GetProcessedRequests();
        }

        public int GetMaxQueueSize()
        {
            return _pr.GetMaxQueueSize();
        }
    }
}