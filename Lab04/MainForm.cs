﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Forms;

namespace Model4
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class MainForm : Form
	{
		private GroupBox gbGenerator;
		private Label label1;
		private Label label2;
		private Label label3;
		private TextBox tbA;
		private TextBox tbB;
		private GroupBox groupBox1;
		private TextBox tbSigma;
		private TextBox tbM;
		private Label label4;
		private Label label5;
		private Label label6;
		private GroupBox groupBox2;
		private Label label8;
		private Label label7;
		private Button button1;
		private TextBox tbReqs;
		private TextBox tbMax;
        private Label label9;
        private TextBox returnProbTb;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		public MainForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.gbGenerator = new System.Windows.Forms.GroupBox();
            this.tbB = new System.Windows.Forms.TextBox();
            this.tbA = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbSigma = new System.Windows.Forms.TextBox();
            this.tbM = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbMax = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbReqs = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.returnProbTb = new System.Windows.Forms.TextBox();
            this.gbGenerator.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbGenerator
            // 
            this.gbGenerator.Controls.Add(this.tbB);
            this.gbGenerator.Controls.Add(this.tbA);
            this.gbGenerator.Controls.Add(this.label3);
            this.gbGenerator.Controls.Add(this.label2);
            this.gbGenerator.Controls.Add(this.label1);
            this.gbGenerator.Location = new System.Drawing.Point(0, 12);
            this.gbGenerator.Name = "gbGenerator";
            this.gbGenerator.Size = new System.Drawing.Size(176, 104);
            this.gbGenerator.TabIndex = 1;
            this.gbGenerator.TabStop = false;
            this.gbGenerator.Text = "Параметры генератора";
            // 
            // tbB
            // 
            this.tbB.Location = new System.Drawing.Point(40, 72);
            this.tbB.Name = "tbB";
            this.tbB.Size = new System.Drawing.Size(128, 20);
            this.tbB.TabIndex = 5;
            this.tbB.Text = "15";
            // 
            // tbA
            // 
            this.tbA.Location = new System.Drawing.Point(40, 48);
            this.tbA.Name = "tbA";
            this.tbA.Size = new System.Drawing.Size(128, 20);
            this.tbA.TabIndex = 4;
            this.tbA.Text = "5";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(16, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = "B";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(16, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "A";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Равномерное распределение:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbSigma);
            this.groupBox1.Controls.Add(this.tbM);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(206, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(176, 104);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Параметры ОА";
            // 
            // tbSigma
            // 
            this.tbSigma.Location = new System.Drawing.Point(48, 72);
            this.tbSigma.Name = "tbSigma";
            this.tbSigma.Size = new System.Drawing.Size(120, 20);
            this.tbSigma.TabIndex = 5;
            this.tbSigma.Text = "5";
            // 
            // tbM
            // 
            this.tbM.Location = new System.Drawing.Point(48, 48);
            this.tbM.Name = "tbM";
            this.tbM.Size = new System.Drawing.Size(120, 20);
            this.tbM.TabIndex = 4;
            this.tbM.Text = "10";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(8, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "σ";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(8, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 16);
            this.label5.TabIndex = 2;
            this.label5.Text = "µ";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(8, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(160, 16);
            this.label6.TabIndex = 1;
            this.label6.Text = "Нормальное распределение:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.returnProbTb);
            this.groupBox2.Controls.Add(this.tbReqs);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(412, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(176, 104);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Диспетчер";
            // 
            // tbMax
            // 
            this.tbMax.Location = new System.Drawing.Point(286, 152);
            this.tbMax.Name = "tbMax";
            this.tbMax.ReadOnly = true;
            this.tbMax.Size = new System.Drawing.Size(160, 20);
            this.tbMax.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(286, 133);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(160, 16);
            this.label7.TabIndex = 5;
            this.label7.Text = "Оптимальная длина очереди";
            // 
            // tbReqs
            // 
            this.tbReqs.Location = new System.Drawing.Point(8, 32);
            this.tbReqs.Name = "tbReqs";
            this.tbReqs.Size = new System.Drawing.Size(160, 20);
            this.tbReqs.TabIndex = 4;
            this.tbReqs.Text = "1000";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(8, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 16);
            this.label8.TabIndex = 2;
            this.label8.Text = "Число заявок";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(142, 150);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Моделировать";
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(8, 55);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(160, 16);
            this.label9.TabIndex = 7;
            this.label9.Text = "Вероятность возврата";
            // 
            // returnProbTb
            // 
            this.returnProbTb.Location = new System.Drawing.Point(8, 72);
            this.returnProbTb.Name = "returnProbTb";
            this.returnProbTb.Size = new System.Drawing.Size(160, 20);
            this.returnProbTb.TabIndex = 8;
            this.returnProbTb.Text = "0.0";
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(673, 194);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbMax);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbGenerator);
            this.Name = "Form1";
            this.Text = "Обслуживающий автомат";
            this.gbGenerator.ResumeLayout(false);
            this.gbGenerator.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new MainForm());
		}

		private void button1_Click_1(object sender, EventArgs e)
		{
			double a, b, m, s, p;
			int q;
			try
			{
                a = Convert.ToDouble(tbA.Text, CultureInfo.InvariantCulture);
                b = Convert.ToDouble(tbB.Text, CultureInfo.InvariantCulture);
                m = Convert.ToDouble(tbM.Text, CultureInfo.InvariantCulture);
                s = Convert.ToDouble(tbSigma.Text, CultureInfo.InvariantCulture);
				q = Convert.ToInt32(tbReqs.Text);
			    p = Convert.ToDouble(returnProbTb.Text, CultureInfo.InvariantCulture);
			}
			catch
			{
			    MessageBox.Show("Некорректный ввод");
				return;
			}

			UniformRandom lr = new UniformRandom(a, b);
			NormalRandom gr = new NormalRandom(s, m);
			Processor pr = new Processor(gr);
			RandomGenerator rg = new RandomGenerator(lr);

			Controller d = new Controller(rg, pr, q, p);
			d.Dispatch();

			tbMax.Text = d.GetMaxQueueSize().ToString();
		}
	}
}
