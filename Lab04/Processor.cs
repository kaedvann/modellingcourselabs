using System.Collections.Generic;

namespace Model4
{
    public class Processor : IGenerator, IReceiver
    {
        private readonly List<IReceiver> _receivers = new List<IReceiver>();
        private readonly BaseRandom _random;
        private int _maxQueueSize;
        private int _qsize;

        private int _totalRequests;

        public Processor(BaseRandom procRandom)
        {
            _random = procRandom;
            _qsize = 0;
            _totalRequests = 0;
            _maxQueueSize = 0;
        }

        public void AddReceiver(IReceiver rcv)
        {
            _receivers.Add(rcv);
        }

        public double NextGenerationPeriod()
        {
            return 0;
        }

        public void SendRequest()
        {
            foreach (IReceiver rcv in _receivers)
                rcv.ReceiveRequest();
        }

        public void ReceiveRequest()
        {
            _qsize++;
            _totalRequests++;
            if (_qsize > _maxQueueSize)
                _maxQueueSize++;
        }

        public double NextProcessingPeriod()
        {
            double r = _random.GenerateNext();
            if (r < 0)
                r = 0;
            return r;
        }

        public void Process()
        {
            if (_qsize > 0)
            {
                _qsize--;
                SendRequest();
            }
        }

        public int GetQueueSize()
        {
            return _qsize;
        }

        public int GetProcessedRequests()
        {
            return _totalRequests;
        }

        public int GetMaxQueueSize()
        {
            return _maxQueueSize;
        }
    }
}