using System.Collections.Generic;

namespace Model4
{
    public class RandomGenerator : IGenerator
    {
        private readonly BaseRandom _random;
        private readonly List<IReceiver> _receivers = new List<IReceiver>();

        public RandomGenerator(BaseRandom genRandom)
        {
            _random = genRandom;
        }

        public void AddReceiver(IReceiver rcvr)
        {
            _receivers.Add(rcvr);
        }

        public double NextGenerationPeriod()
        {
            double r = _random.GenerateNext();
            if (r < 0)
                r = 0;
            return r;
        }

        public void SendRequest()
        {
            foreach (IReceiver rcv in _receivers)
                rcv.ReceiveRequest();
        }
    }
}