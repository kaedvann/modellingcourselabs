namespace Model4
{
	public interface IReceiver
	{
		void ReceiveRequest();
		double NextProcessingPeriod();
		void Process();
	}
}
