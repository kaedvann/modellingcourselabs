using System;

namespace Model4
{
    public abstract class BaseRandom
    {
        private readonly Random _random;

        public BaseRandom(int seed)
        {
            _random = new Random(seed);
        }

        public BaseRandom()
        {
            _random = new Random(0);
        }

        protected double GenerateNormalized()
        {
            return _random.NextDouble();
        }

        public abstract double GenerateNext();
    }
}