﻿namespace lab6
{
    public class GaussRandom : BaseRandom
    {
        public GaussRandom(double m, double sigma)
            : base(0)
        {
            this.sigma = sigma;
            this.m = m;
        }

        public GaussRandom(double sigma, double m, int seed)
            : base(seed)
        {
            this.sigma = sigma;
            this.m = m;
        }

        public override double GenerateNext()
        {
            double sum = 0;
            for (var i = 0; i < 12; i++)
                sum += this.GenerateNormalized();
            return sigma * (sum - 6) + m;
        }

        private double sigma, m;
    }
}