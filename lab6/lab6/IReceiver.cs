﻿namespace lab6
{
    public interface IReceiver
    {
        int QueueSize { get; }
        void ReceiveRequest(double r);
        double NextProcessingPeriod();
        bool canReceive();
    }
}