﻿using System;
using System.Collections.Generic;

namespace lab6
{

    public class RandomGenerator
    {
        private BaseRandom _random;
        private readonly List<IReceiver> _receivers = new List<IReceiver>();
        private int _requests;
        private int _groupSize;
        private readonly  Random _rand = new Random();

        public int RequestsCount
        {
            get { return _requests; }
        }

        public RandomGenerator(BaseRandom genRandom, int requests, int groupSize = 1)
        {
            _random = genRandom;
            _requests = requests;
            _groupSize = groupSize;
        }

        public void BindReceiver(IReceiver rcvr)
        {
            _receivers.Add(rcvr);
        }

        public void BindReceivers(IEnumerable<IReceiver> rcvr)
        {
            _receivers.AddRange(rcvr);
        }

        public double NextGenerationPeriod()
        {
            var r = _random.GenerateNext();
            if (r < 0)
                r = 0;
            return r;
        }

        public void SendRequestToAll(double r)
        {
            --_requests;
            foreach (IReceiver rcv in _receivers)
                rcv.ReceiveRequest(r);
        }

        public void SendRequestWithEqualProbability(double r)
        {
            --_requests;
            var prob = 1.0/_receivers.Count;
            var res = _rand.NextDouble();
            var index = (int) (res/prob);
            var rcv = (IReceiver) _receivers[index];
            rcv.ReceiveRequest(r);
        }

        public int SendWhileNotReceived(double r)
        {
            --_requests;
            IReceiver rcv;
            for (var i = 0; i < _receivers.Count; i++)
            {
                rcv = (IReceiver)_receivers[i];
                if (rcv.canReceive())
                {
                    rcv.ReceiveRequest(r);
                    return i;
                }
            }
            return -1;
        }

        public int SendToMinQueueSize(double r)
        {
            var res = -1;
            if (_requests > 0)
            {
                var rcv = (IReceiver)_receivers[0];
                for (var i = 0; i < _receivers.Count; i++)
                {
                    var tmp = (IReceiver)_receivers[i];
                    if (tmp.QueueSize <= rcv.QueueSize)
                    {
                        rcv = tmp;
                        res = i;
                    }
                }

                if (rcv.canReceive())
                {
                    --_requests;
                    rcv.ReceiveRequest(r);
                }
            }
            return res;
        }

        public void SendGroup(double r)
        {
            for (var i = 0; i < _groupSize; ++i)
            {
                SendWhileNotReceived(r);
            }
        }
    }
}
