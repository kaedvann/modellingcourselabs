﻿using System.Linq;

namespace lab6
{
    public class Controller
    {
        private double _time;
        private double[] _times;
        public Cashier Cashier1 { get; set; }
        public Cashier Cashier2 { get; set; }
        public Cashier Cashier3 { get; set; }
        public TicketChecker TicketChecker1 { get; set; }
        public TicketChecker TicketChecker2 { get; set; }
        public TicketChecker TicketChecker3 { get; set; }
        public TicketChecker TicketChecker4 { get; set; }
        public TicketChecker TicketChecker5 { get; set; }
        public Train Train { get; set; }

        public double ModellingTime
        {
            get { return _time; }
        }

        public TicketChecker TicketChecker6 { get; set; }

        public void Manage()
        {
            var genEnter1 = new RandomGenerator(new GaussRandom(Cashier1.M, Cashier1.Sigma), Cashier1.Requests);
            var genEnter2 = new RandomGenerator(new GaussRandom(Cashier2.M, Cashier2.Sigma), Cashier2.Requests);
            var genEnter3 = new RandomGenerator(new GaussRandom(Cashier3.M, Cashier3.Sigma), Cashier3.Requests);
            var prTurnstile1 = new Processor(new UniformRandom(TicketChecker1.A, TicketChecker1.B));
            var prTurnstile2 = new Processor(new UniformRandom(TicketChecker2.A, TicketChecker2.B));
            var prTurnstile3 = new Processor(new UniformRandom(TicketChecker3.A, TicketChecker3.B));
            var prTurnstile4 = new Processor(new UniformRandom(TicketChecker4.A, TicketChecker4.B));
            var prTurnstile5 = new Processor(new UniformRandom(TicketChecker5.A, TicketChecker5.B));
            var prTurnstile6 = new Processor(new UniformRandom(TicketChecker6.A, TicketChecker6.B));
            var prTrain1 = new Processor(new GaussRandom(Train.M, Train.Sigma), 300, 200);
            var exit = new Receiver();

            genEnter1.BindReceivers(new IReceiver[] {prTurnstile1, prTurnstile2, prTurnstile3, prTurnstile4, prTurnstile5, prTurnstile6});
            genEnter2.BindReceivers(new IReceiver[] {prTurnstile1, prTurnstile2, prTurnstile3, prTurnstile4, prTurnstile5, prTurnstile6});
            genEnter3.BindReceivers(new IReceiver[] {prTurnstile1, prTurnstile2, prTurnstile3, prTurnstile4, prTurnstile5, prTurnstile6});
            prTurnstile1.BindReceiver(prTrain1);
            prTurnstile2.BindReceiver(prTrain1);
            prTurnstile3.BindReceiver(prTrain1);
            prTurnstile4.BindReceiver(prTrain1);
            prTurnstile5.BindReceiver(prTrain1);
            prTurnstile6.BindReceiver(prTrain1);
            prTrain1.BindReceiver(exit);

            _times = new[]
            {
                genEnter1.NextGenerationPeriod(),
                genEnter2.NextGenerationPeriod(),
                genEnter3.NextGenerationPeriod(),
                prTurnstile1.NextProcessingPeriod(),
                prTurnstile2.NextProcessingPeriod(),
                prTurnstile3.NextProcessingPeriod(),
                prTurnstile4.NextProcessingPeriod(),
                prTurnstile5.NextProcessingPeriod(),
                prTurnstile6.NextProcessingPeriod(),
                prTrain1.NextProcessingPeriod()
            };

            _time = 0;
            int sumRequests = Cashier1.Requests + Cashier2.Requests + Cashier3.Requests;

            while (exit.QueueSize < sumRequests)
            {
                _time = _times.Min();
                if (_times[0] == _time)
                {
                    genEnter1.SendToMinQueueSize(_time);
                    _times[0] += genEnter1.NextGenerationPeriod();
                    continue;
                }
                if (_times[1] == _time)
                {
                    genEnter2.SendToMinQueueSize(_time);
                    _times[1] += genEnter2.NextGenerationPeriod();
                    continue;
                }
                if (_times[2] == _time)
                {
                    genEnter3.SendToMinQueueSize(_time);
                    _times[2] += genEnter3.NextGenerationPeriod();
                    continue;
                }
                if (_times[3] == _time)
                {
                    prTurnstile1.SendRequestWithEqualProbability(_time);
                    _times[3] += prTurnstile1.NextProcessingPeriod();
                    continue;
                }
                if (_times[4] == _time)
                {
                    prTurnstile2.SendRequestWithEqualProbability(_time);
                    _times[4] += prTurnstile2.NextProcessingPeriod();
                    continue;
                }
                if (_times[5] == _time)
                {
                    prTurnstile3.SendRequestWithEqualProbability(_time);
                    _times[5] += prTurnstile3.NextProcessingPeriod();
                    continue;
                }
                if (_times[6] == _time)
                {
                    prTurnstile4.SendRequestWithEqualProbability(_time);
                    _times[6] += prTurnstile4.NextProcessingPeriod();
                    continue;
                }
                if (_times[7] == _time)
                {
                    prTurnstile5.SendRequestWithEqualProbability(_time);
                    _times[7] += prTurnstile5.NextProcessingPeriod();
                    continue;
                }
                if (_times[8] == _time)
                {
                    prTurnstile6.SendRequestWithEqualProbability(_time);
                    _times[8] += prTurnstile6.NextProcessingPeriod();
                    continue;
                }
                if (_times[9] == _time)
                {
                    prTrain1.SendGroup(_time);
                    _times[9] += prTrain1.NextProcessingPeriod();
                }
            }
            _time = _times.Min();
            TicketChecker1.MaxWaitingTime = prTurnstile1.MaxWaitingTime;
            TicketChecker2.MaxWaitingTime = prTurnstile2.MaxWaitingTime;
            TicketChecker3.MaxWaitingTime = prTurnstile3.MaxWaitingTime;
            TicketChecker4.MaxWaitingTime = prTurnstile4.MaxWaitingTime;
            TicketChecker5.MaxWaitingTime = prTurnstile5.MaxWaitingTime;
            TicketChecker6.MaxWaitingTime = prTurnstile6.MaxWaitingTime;
            Train.MaxWaitingTime = prTrain1.MaxWaitingTime;
        }
    }
}