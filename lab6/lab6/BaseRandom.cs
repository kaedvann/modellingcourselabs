﻿using System;

namespace lab6
{
    public abstract class BaseRandom
    {
        public BaseRandom(int seed)
        {
            random = new Random(seed);
        }

        public BaseRandom()
        {
            random = new Random(0);
        }

        public double GenerateNormalized()
        {
            return random.NextDouble();
        }

        public abstract double GenerateNext();

        private Random random;
    }
}
