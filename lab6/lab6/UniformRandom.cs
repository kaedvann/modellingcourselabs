﻿namespace lab6
{
    public class UniformRandom : BaseRandom
    {
        public UniformRandom(double a, double b)
            : base(0)
        {
            this.a = a;
            this.b = b;
        }

        public UniformRandom(double a, double b, int seed)
            : base(seed)
        {
            this.a = a;
            this.b = b;
        }

        public override double GenerateNext()
        {
            return (b - a) * GenerateNormalized() + a;
        }

        private double a, b;
    }
}