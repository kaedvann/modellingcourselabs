﻿namespace lab6
{
    partial class TrainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnModel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.edtTs1 = new System.Windows.Forms.TextBox();
            this.edtTs2 = new System.Windows.Forms.TextBox();
            this.edtTs3 = new System.Windows.Forms.TextBox();
            this.edtTs4 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.edtTs5 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.edtTr1 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.edtEnt3 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.edtEnt1 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.edtEnt2 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.edtModelTime = new System.Windows.Forms.TextBox();
            this.edtTs6 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnModel
            // 
            this.btnModel.Location = new System.Drawing.Point(18, 289);
            this.btnModel.Margin = new System.Windows.Forms.Padding(4);
            this.btnModel.Name = "btnModel";
            this.btnModel.Size = new System.Drawing.Size(147, 33);
            this.btnModel.TabIndex = 0;
            this.btnModel.Text = "Провести расчет";
            this.btnModel.UseVisualStyleBackColor = true;
            this.btnModel.Click += new System.EventHandler(this.btnModel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 19);
            this.label1.TabIndex = 1;
            this.label1.Text = "Турникет1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 19);
            this.label2.TabIndex = 2;
            this.label2.Text = "Турникет2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 19);
            this.label3.TabIndex = 3;
            this.label3.Text = "Турникет3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(269, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 19);
            this.label4.TabIndex = 4;
            this.label4.Text = "Турникет4";
            // 
            // edtTs1
            // 
            this.edtTs1.Enabled = false;
            this.edtTs1.Location = new System.Drawing.Point(102, 31);
            this.edtTs1.Name = "edtTs1";
            this.edtTs1.Size = new System.Drawing.Size(147, 26);
            this.edtTs1.TabIndex = 5;
            // 
            // edtTs2
            // 
            this.edtTs2.Enabled = false;
            this.edtTs2.Location = new System.Drawing.Point(102, 65);
            this.edtTs2.Name = "edtTs2";
            this.edtTs2.Size = new System.Drawing.Size(147, 26);
            this.edtTs2.TabIndex = 6;
            // 
            // edtTs3
            // 
            this.edtTs3.Enabled = false;
            this.edtTs3.Location = new System.Drawing.Point(102, 98);
            this.edtTs3.Name = "edtTs3";
            this.edtTs3.Size = new System.Drawing.Size(147, 26);
            this.edtTs3.TabIndex = 7;
            // 
            // edtTs4
            // 
            this.edtTs4.Enabled = false;
            this.edtTs4.Location = new System.Drawing.Point(354, 31);
            this.edtTs4.Name = "edtTs4";
            this.edtTs4.Size = new System.Drawing.Size(147, 26);
            this.edtTs4.TabIndex = 8;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.edtTs6);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.edtTs5);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.edtTr1);
            this.groupBox1.Controls.Add(this.edtTs4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.edtTs3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.edtTs2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.edtTs1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(18, 113);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(723, 146);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Максимальное время в очереди";
            // 
            // edtTs5
            // 
            this.edtTs5.Enabled = false;
            this.edtTs5.Location = new System.Drawing.Point(354, 63);
            this.edtTs5.Name = "edtTs5";
            this.edtTs5.Size = new System.Drawing.Size(147, 26);
            this.edtTs5.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(269, 66);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 19);
            this.label7.TabIndex = 13;
            this.label7.Text = "Турникет5";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(515, 34);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 19);
            this.label5.TabIndex = 9;
            this.label5.Text = "Поезд";
            // 
            // edtTr1
            // 
            this.edtTr1.Enabled = false;
            this.edtTr1.Location = new System.Drawing.Point(570, 31);
            this.edtTr1.Name = "edtTr1";
            this.edtTr1.Size = new System.Drawing.Size(147, 26);
            this.edtTr1.TabIndex = 11;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.edtEnt3);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.edtEnt1);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.edtEnt2);
            this.groupBox2.Location = new System.Drawing.Point(18, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(723, 97);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Количество людей:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(508, 28);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 19);
            this.label10.TabIndex = 19;
            this.label10.Text = "Касса3";
            // 
            // edtEnt3
            // 
            this.edtEnt3.Location = new System.Drawing.Point(570, 25);
            this.edtEnt3.Name = "edtEnt3";
            this.edtEnt3.Size = new System.Drawing.Size(147, 26);
            this.edtEnt3.TabIndex = 21;
            this.edtEnt3.Text = "1000";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(40, 28);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 19);
            this.label8.TabIndex = 15;
            this.label8.Text = "Касса1";
            // 
            // edtEnt1
            // 
            this.edtEnt1.Location = new System.Drawing.Point(102, 25);
            this.edtEnt1.Name = "edtEnt1";
            this.edtEnt1.Size = new System.Drawing.Size(147, 26);
            this.edtEnt1.TabIndex = 17;
            this.edtEnt1.Text = "1000";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(277, 28);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 19);
            this.label9.TabIndex = 16;
            this.label9.Text = "Касса2";
            // 
            // edtEnt2
            // 
            this.edtEnt2.Location = new System.Drawing.Point(339, 25);
            this.edtEnt2.Name = "edtEnt2";
            this.edtEnt2.Size = new System.Drawing.Size(147, 26);
            this.edtEnt2.TabIndex = 18;
            this.edtEnt2.Text = "1000";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(418, 296);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(164, 19);
            this.label12.TabIndex = 12;
            this.label12.Text = "Время моделирования:";
            // 
            // edtModelTime
            // 
            this.edtModelTime.Enabled = false;
            this.edtModelTime.Location = new System.Drawing.Point(588, 293);
            this.edtModelTime.Name = "edtModelTime";
            this.edtModelTime.Size = new System.Drawing.Size(147, 26);
            this.edtModelTime.TabIndex = 13;
            // 
            // edtTs6
            // 
            this.edtTs6.Enabled = false;
            this.edtTs6.Location = new System.Drawing.Point(354, 98);
            this.edtTs6.Name = "edtTs6";
            this.edtTs6.Size = new System.Drawing.Size(147, 26);
            this.edtTs6.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(269, 101);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 19);
            this.label6.TabIndex = 15;
            this.label6.Text = "Турникет6";
            // 
            // TrainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(753, 361);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.edtModelTime);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnModel);
            this.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "TrainForm";
            this.Text = "Конечная станция электрички";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnModel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox edtTs1;
        private System.Windows.Forms.TextBox edtTs2;
        private System.Windows.Forms.TextBox edtTs3;
        private System.Windows.Forms.TextBox edtTs4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox edtTs5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox edtTr1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox edtEnt3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox edtEnt1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox edtEnt2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox edtModelTime;
        private System.Windows.Forms.TextBox edtTs6;
        private System.Windows.Forms.Label label6;
    }
}

