﻿using System;
using System.Collections.Generic;

namespace lab6
{
    public class Processor : IReceiver
    {
        private BaseRandom _random;
        private readonly List<IReceiver> _receivers = new List<IReceiver>();
        private readonly List<double> _queue = new List<double>();
        private readonly Random _rand = new Random();
        private bool isBufferLimited;

        private int _totalRequests;
        private int _maxQueueSize;
        private double _maxWaitingTime;
        private int _groupSize;

        public int QueueSize
        {
            get { return _queue.Count; }
        }

        public int ProcessedRequests
        {
            get { return _totalRequests; }
        }

        public int MaxQueueSize
        {
            get { return _maxQueueSize; }
        }

        public double MaxWaitingTime
        {
            get { return _maxWaitingTime; }
            set { _maxWaitingTime = (_maxWaitingTime < value) ? value : _maxWaitingTime; }
        }

        public Processor(BaseRandom procRandom, int maxQueueSize = 0, int groupSize = 1)
        {
            _random = procRandom;
            _groupSize = groupSize;

            if (maxQueueSize == 0)
                isBufferLimited = false;
            else
                _maxQueueSize = maxQueueSize;

            _totalRequests = 0;
            _maxWaitingTime = 0;
        }

        public void BindReceiver(IReceiver rcv)
        {
            _receivers.Add(rcv);
        }
        
        public void BindReceivers(IReceiver[] rcvr)
        {
            _receivers.AddRange(rcvr);
        }

        public double NextGenerationPeriod()
        {
            var r = _random.GenerateNext();
            if (r < 0)
                r = 0;
            return r;
        }

        public void SendRequestToAll(double r)
        {
            foreach (IReceiver rcv in _receivers)
                rcv.ReceiveRequest(r);
        }

        private void SendRequest(IReceiver rcv, double r)
        {
            MaxWaitingTime = r - _queue[0];
            _queue.RemoveAt(0);
            rcv.ReceiveRequest(r);
        }

        public void SendRequestWithEqualProbability(double r)
        {
            if (QueueSize > 0)
            {
                var prob = 1.0 / _receivers.Count;
                var res = _rand.NextDouble();
                var index = (int)(res / prob);
                var rcv = _receivers[index];
                SendRequest(rcv, r);
            }
        }

        public int SendWhileNotReceived(double r)
        {
            if (QueueSize > 0)
            {
                IReceiver rcv;
                for (var i = 0; i < _receivers.Count; i++)
                {
                    rcv = _receivers[i];
                    if (rcv.canReceive())
                    {
                        SendRequest(rcv, r);
                        return i;
                    }
                }
            }
            return -1;
        }

        public int SendToMinQueueSize(double r)
        {
            var res = -1;
            if (QueueSize > 0)
            {
                var rcv = _receivers[0];
                for (var i = 0; i < _receivers.Count; i++)
                {
                    var tmp = _receivers[i];
                    if (tmp.QueueSize <= rcv.QueueSize)
                    {
                        rcv = tmp;
                        res = i;
                    }
                }

                if (rcv.canReceive())
                {
                    SendRequest(rcv, r);
                }
            }
            return res;
        }

        public void SendGroup(double r)
        {
            for (var i = 0; i < _groupSize && QueueSize > 0; ++i)
            {
                SendWhileNotReceived(r);
            }
        }

        public void ReceiveRequest(double r)
        {
            _queue.Add(r);
            ++_totalRequests;
            if (!isBufferLimited && QueueSize > _maxQueueSize)
                _maxQueueSize = QueueSize;
        }

        public double NextProcessingPeriod()
        {
            var r = _random.GenerateNext();
            if (r < 0)
                r = 0;
            return r;
        }

        public bool canReceive()
        {
            return !isBufferLimited ? true : QueueSize < _maxQueueSize;
        }
    }
}
