namespace lab6
{
    public class Train
    {
        public int M { get; set; }
        public int Sigma { get; set; }
        public double MaxWaitingTime { get; set; }
    }
}