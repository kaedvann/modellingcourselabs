﻿using System;
using System.Windows.Forms;

namespace lab6
{
    public partial class TrainForm : Form
    {
        public TrainForm()
        {
            InitializeComponent();
        }

        private Cashier _enter1;
        private Cashier _enter2;
        private Cashier _enter3;
        private TicketChecker _ticketChecker1;
        private TicketChecker _ticketChecker2;
        private TicketChecker _ticketChecker3;
        private TicketChecker _ticketChecker4;
        private TicketChecker _ticketChecker5;
        private Train _train;
        private TicketChecker _ticketChecker6;

        private void btnModel_Click(object sender, EventArgs e)
        {
            try
            {
                _enter1 = new Cashier { Requests = int.Parse(edtEnt1.Text), M = 5, Sigma = 1 };
                _enter2 = new Cashier { Requests = int.Parse(edtEnt2.Text), M = 5, Sigma = 2 };
                _enter3 = new Cashier { Requests = int.Parse(edtEnt3.Text), M = 5, Sigma = 3 };
                _ticketChecker1 = new TicketChecker { A = 1, B = 2 };
                _ticketChecker2 = new TicketChecker { A = 1, B = 3 };
                _ticketChecker3 = new TicketChecker { A = 1, B = 4 };
                _ticketChecker4 = new TicketChecker { A = 1, B = 5 };
                _ticketChecker5 = new TicketChecker { A = 1, B = 6 };
                _ticketChecker6 = new TicketChecker {A = 3, B = 5};
                _train = new Train { M = 50, Sigma = 15 };
            }
            catch (FormatException ex)
            {
                MessageBox.Show(ex.Message);
            }

            var man = new Controller
            {
                Cashier1 = _enter1,
                Cashier2 = _enter2,
                Cashier3 = _enter3,
                TicketChecker1 = _ticketChecker1,
                TicketChecker2 = _ticketChecker2,
                TicketChecker3 = _ticketChecker3,
                TicketChecker4 = _ticketChecker4,
                TicketChecker5 = _ticketChecker5,
                TicketChecker6 = _ticketChecker6,
                Train = _train,
            };

            man.Manage();
            edtTs1.Text = _ticketChecker1.MaxWaitingTime.ToString("G5");
            edtTs2.Text = _ticketChecker2.MaxWaitingTime.ToString("G5");
            edtTs3.Text = _ticketChecker3.MaxWaitingTime.ToString("G5");
            edtTs4.Text = _ticketChecker4.MaxWaitingTime.ToString("G5");
            edtTs5.Text = _ticketChecker5.MaxWaitingTime.ToString("G5");
            edtTs6.Text = _ticketChecker6.MaxWaitingTime.ToString("G5");
            edtModelTime.Text = man.ModellingTime.ToString("G5");
            edtTr1.Text = _train.MaxWaitingTime.ToString("G5");
        }
    }
}
