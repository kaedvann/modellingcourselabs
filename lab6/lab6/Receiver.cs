﻿namespace lab6
{
    public class Receiver : IReceiver
    {
        private int _qsize;

        public int QueueSize
        {
            get { return _qsize; }
        }

        public void ReceiveRequest(double r)
        {
            ++_qsize;
        }

        public double NextProcessingPeriod()
        {
            return 0;
        }

        public bool canReceive()
        {
            return true;
        }
    }
}
