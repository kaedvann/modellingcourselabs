namespace ModellingLab05
{
	public interface IGenerator
	{
		void AddReceiver(IReceiver rcvr);
		double NextGenerationPeriod();
		void SendRequest(object r);
		int SendRequestExclusive(object r);
	}
}
