using System;
using System.Collections.Generic;

namespace ModellingLab05
{
    public enum ProcessorType
    {
        Operator1, Operator2, Operator3, Computer1, Computer2, Empty
    }
	public class Controller
	{
	    readonly LineRandom _lr = new LineRandom(8, 12);
	    readonly LineRandom _lrOp1 = new LineRandom(15, 25);
	    readonly LineRandom _lrOp2 = new LineRandom(30, 50);
	    readonly LineRandom _lrOp3 = new LineRandom(20, 60);
        Processor _op1;
        Processor _op2;
        Processor _op3;
        Processor _comp1;
        Processor _comp2;
        private double _time;
        private int _dropped;
        private int _maxqsize;
	    private const double Tolerance = 0.0001;
        private Dictionary<ProcessorType, double> procTimes = new Dictionary<ProcessorType, double>(6);
        private Dictionary<ProcessorType, Action<double>> procActions = new Dictionary<ProcessorType, Action<double>>(6);
        RandomGenerator _gen;

	    public ProcessorType FindMin()
	    {
	        double tmp = procTimes[ProcessorType.Empty];
	        var res = ProcessorType.Empty;
            foreach (var p in procTimes)
            {
                if (p.Value <  tmp && Math.Abs(p.Value) > Tolerance)
                    res = p.Key;
            }
	        return res;
	    }

		public void Dispatch(int reqs)
		{
            _gen = new RandomGenerator(_lr);
            _op1 = new Processor(_lrOp1, 1);
            _op2 = new Processor(_lrOp2, 1);
            _op3 = new Processor(_lrOp3, 1);
			LineRandom lrComp1 = new LineRandom(15,15);
			LineRandom lrComp2 = new LineRandom(30,30);
			_comp1 = new Processor(lrComp1, 999999);
			_comp2 = new Processor(lrComp2, 999999);
		    
			_gen.AddReceiver(_op1);
			_gen.AddReceiver(_op2);
			_gen.AddReceiver(_op3);
			_op1.AddReceiver(_comp1);
			_op2.AddReceiver(_comp1);
			_op3.AddReceiver(_comp2);
            SetActions();
			_time = 0;
			_dropped = 0;
			

			_gen.SendRequestExclusive(null);
            procTimes[ProcessorType.Empty] = _gen.NextGenerationPeriod();
            procTimes[ProcessorType.Operator1] = _op1.NextProcessingPeriod();
			do
			{
			    ProcessorType currentProc = FindMin();
			    double currentTime = procTimes[currentProc];
			    procActions[currentProc](currentTime);
				_time = currentTime;
				
			}
			while (_gen.Requests() < reqs);
			_maxqsize = _comp1.GetMaxQueueSize() + _comp2.GetMaxQueueSize();
		}

		public int Dropped()
		{
			return _dropped;
		}

		public int MaxQSize()
		{
			return _maxqsize;
		}



	    private void SetActions()
	    {
            procActions[ProcessorType.Operator1] = currentTime =>
	        {
                if (_comp1.GetQueueSize() == 0)
                    procTimes[ProcessorType.Computer1] = currentTime + _comp1.NextProcessingPeriod();
                _op1.Release();
                procTimes[ProcessorType.Operator1] = 0;
	        };
            procActions[ProcessorType.Operator2] = currentTime =>
            {
                if (_comp1.GetQueueSize() == 0)
                    procTimes[ProcessorType.Computer1] = currentTime + _comp1.NextProcessingPeriod();
                _op2.Release();
                procTimes[ProcessorType.Operator2] = 0;
            };
            procActions[ProcessorType.Operator3] = currentTime =>
            {
                if (_comp2.GetQueueSize() == 0)
                    procTimes[ProcessorType.Computer2] = currentTime + _comp2.NextProcessingPeriod();
                _op3.Release();
                procTimes[ProcessorType.Operator3] = 0;
            };
            procActions[ProcessorType.Computer1] = currentTime =>
            {
                _comp1.Release();
                if (_comp1.GetQueueSize() == 0)
                    procTimes[ProcessorType.Computer1] = 0;
                else
                {
                    procTimes[ProcessorType.Computer1] = currentTime + _comp1.NextProcessingPeriod();
                }
            };
            procActions[ProcessorType.Computer2] = currentTime =>
            {
                _comp2.Release();
                if (_comp2.GetQueueSize() == 0)
                    procTimes[ProcessorType.Computer2] = 0;
                else
                {
                    procTimes[ProcessorType.Computer2] = currentTime + _comp2.NextProcessingPeriod();
                }
            };
            procActions[ProcessorType.Empty] = currentTime =>
            {
                int dev = _gen.SendRequestExclusive(null);
                switch (dev)
                {
                    case 0:
                        procTimes[ProcessorType.Operator1] = currentTime + _op1.NextProcessingPeriod();
                        break;
                    case 1:
                        procTimes[ProcessorType.Operator2] = currentTime + _op2.NextProcessingPeriod();
                        break;
                    case 2:
                        procTimes[ProcessorType.Operator3] = currentTime + _op3.NextProcessingPeriod();
                        break;
                    case -1:
                        _dropped++;
                        break;
                }
                procTimes[ProcessorType.Empty] = currentTime + _gen.NextGenerationPeriod();
            };
	    }
		

	}
}
