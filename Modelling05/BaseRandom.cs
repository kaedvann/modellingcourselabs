using System;

namespace ModellingLab05
{
	public abstract class BaseRandom
	{
		public BaseRandom(int seed)
		{
			random = new Random(seed);
		}

		public BaseRandom()
		{
			random = new Random(0);
		}

		protected double GenerateNormalized()
		{
			return random.NextDouble();
		}

		public abstract double GenerateNext();

		private Random random;
	}
}
