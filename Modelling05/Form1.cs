using System;
using System.Windows.Forms;

namespace ModellingLab05
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmMain : System.Windows.Forms.Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox tbReqs;
		private System.Windows.Forms.TextBox tbProb;
		private System.Windows.Forms.TextBox tbQSize;
		private System.Windows.Forms.Button button1;
		private Controller d;
		public frmMain()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			d = new Controller();
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbReqs = new System.Windows.Forms.TextBox();
            this.tbProb = new System.Windows.Forms.TextBox();
            this.tbQSize = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(13, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(179, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "���������� ������:";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(234, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(192, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "����������� ������:";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(478, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(269, 24);
            this.label3.TabIndex = 2;
            this.label3.Text = "����� �������:";
            // 
            // tbReqs
            // 
            this.tbReqs.Location = new System.Drawing.Point(18, 42);
            this.tbReqs.Name = "tbReqs";
            this.tbReqs.Size = new System.Drawing.Size(160, 26);
            this.tbReqs.TabIndex = 3;
            this.tbReqs.Text = "300";
            // 
            // tbProb
            // 
            this.tbProb.Location = new System.Drawing.Point(234, 94);
            this.tbProb.Name = "tbProb";
            this.tbProb.ReadOnly = true;
            this.tbProb.Size = new System.Drawing.Size(165, 26);
            this.tbProb.TabIndex = 4;
            // 
            // tbQSize
            // 
            this.tbQSize.Location = new System.Drawing.Point(478, 94);
            this.tbQSize.Name = "tbQSize";
            this.tbQSize.ReadOnly = true;
            this.tbQSize.Size = new System.Drawing.Size(165, 26);
            this.tbQSize.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 91);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(165, 29);
            this.button1.TabIndex = 6;
            this.button1.Text = "���������";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // frmMain
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(8, 19);
            this.ClientSize = new System.Drawing.Size(684, 176);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tbQSize);
            this.Controls.Add(this.tbProb);
            this.Controls.Add(this.tbReqs);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "frmMain";
            this.Text = "Mod5";
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new frmMain());
		}


		private void button1_Click(object sender, System.EventArgs e)
		{
			int reqs;
			reqs = Convert.ToInt32(tbReqs.Text);
			d.Dispatch(reqs);
			tbProb.Text = (d.Dropped()/(double)reqs).ToString();
			tbQSize.Text = d.MaxQSize().ToString();
		}
	}
}
