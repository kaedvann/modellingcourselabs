namespace ModellingLab05
{
	public interface IReceiver
	{
		void ReceiveRequest(object r);
		double NextProcessingPeriod();
		void Release();
		bool CanReceive();
	}
}
