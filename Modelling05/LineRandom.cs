namespace ModellingLab05
{
	public class LineRandom : BaseRandom
	{
		public LineRandom(double a, double b) : base(0)
		{
			this.a = a;
			this.b = b;
		}

		public LineRandom(double a, double b, int seed) : base(seed)
		{
			this.a = a;
			this.b = b;
		}

		public override double GenerateNext()
		{
			return (b - a)*GenerateNormalized() + a;
		}

		private double a, b;
	}
}
