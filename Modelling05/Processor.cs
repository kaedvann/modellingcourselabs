using System.Collections;

namespace ModellingLab05
{
    public class Processor : IGenerator, IReceiver
    {
        private readonly int mqsize;
        private readonly BaseRandom random;
        private readonly ArrayList receivers;
        private int maxqsize;
        private int qsize;
        private int totalRequests;

        public Processor(BaseRandom procRandom, int MaxQueueSize)
        {
            random = procRandom;
            receivers = new ArrayList();
            mqsize = MaxQueueSize;
            maxqsize = 0;
        }

        public void AddReceiver(IReceiver rcv)
        {
            receivers.Add(rcv);
        }

        public double NextGenerationPeriod()
        {
            return 0;
        }

        public void SendRequest(object r)
        {
            foreach (IReceiver rcv in receivers)
                if (rcv.CanReceive())
                    rcv.ReceiveRequest(r);
        }

        public int SendRequestExclusive(object r)
        {
            IReceiver rcv;
            for (int i = 0; i < receivers.Count; i++)
            {
                rcv = (IReceiver) receivers[i];
                if (rcv.CanReceive())
                {
                    rcv.ReceiveRequest(r);
                    return i;
                }
            }
            return -1;
        }

        public void ReceiveRequest(object r)
        {
            if (CanReceive())
            {
                qsize++;
                if (qsize > maxqsize)
                    maxqsize = qsize;
            }
        }

        public double NextProcessingPeriod()
        {
            double r = random.GenerateNext();
            if (r < 0)
                r = 0;
            return r;
        }

        public void Release()
        {
            if (qsize > 0)
            {
                qsize--;
                SendRequest(null);
            }
        }

        public bool CanReceive()
        {
            return (qsize < mqsize);
        }

        public int GetQueueSize()
        {
            return qsize;
        }

        public int GetProcessedRequests()
        {
            return totalRequests;
        }

        public int GetMaxQueueSize()
        {
            return maxqsize;
        }
    }
}