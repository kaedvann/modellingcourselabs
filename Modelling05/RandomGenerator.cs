using System.Collections;

namespace ModellingLab05
{

	public class RandomGenerator : IGenerator
	{
		public RandomGenerator(BaseRandom genRandom)
		{
			random = genRandom;
			receivers = new ArrayList();
			requests = 0;
		}

		public void AddReceiver(IReceiver rcvr)
		{
			receivers.Add(rcvr);
		}

		public double NextGenerationPeriod()
		{
			double r = random.GenerateNext();
			if (r < 0)
				r = 0;
			return r;
		}

		public void SendRequest(object r)
		{
			requests++;
			foreach (IReceiver rcv in receivers)
				if (rcv.CanReceive())
					rcv.ReceiveRequest(r);
		}

		public int SendRequestExclusive(object r)
		{
			requests++;
			IReceiver rcv;
			for (int i = 0; i < receivers.Count; i++)
			{
				rcv = (IReceiver)receivers[i];
				if (rcv.CanReceive())
				{
					rcv.ReceiveRequest(r);
					return i;
				}
			}
			return -1;
		}

		public int Requests()
		{
			return requests;
		}

		private BaseRandom random;
		private ArrayList receivers;
		private int requests;
	}
}
