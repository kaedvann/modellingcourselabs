﻿namespace Lab01
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AlgGrid = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TableGrid = new System.Windows.Forms.DataGridView();
            this.AlgCriteriaGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TableCriteriaGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UserInputGrid = new System.Windows.Forms.DataGridView();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BtnResetTables = new System.Windows.Forms.Button();
            this.TbResult = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SetUserSizeButton = new System.Windows.Forms.Button();
            this.TbUserSize = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.EvaluateUserInputButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.AlgGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TableGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AlgCriteriaGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TableCriteriaGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserInputGrid)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // AlgGrid
            // 
            this.AlgGrid.AllowUserToAddRows = false;
            this.AlgGrid.AllowUserToDeleteRows = false;
            this.AlgGrid.AllowUserToResizeColumns = false;
            this.AlgGrid.AllowUserToResizeRows = false;
            this.AlgGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.AlgGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.AlgGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.AlgGrid.Location = new System.Drawing.Point(0, 65);
            this.AlgGrid.Name = "AlgGrid";
            this.AlgGrid.ReadOnly = true;
            this.AlgGrid.RowHeadersVisible = false;
            this.AlgGrid.RowTemplate.Height = 28;
            this.AlgGrid.Size = new System.Drawing.Size(430, 429);
            this.AlgGrid.TabIndex = 0;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "1 разряд";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 102;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "2 разряда";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 111;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "3 разряда";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 111;
            // 
            // TableGrid
            // 
            this.TableGrid.AllowUserToAddRows = false;
            this.TableGrid.AllowUserToDeleteRows = false;
            this.TableGrid.AllowUserToResizeColumns = false;
            this.TableGrid.AllowUserToResizeRows = false;
            this.TableGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.TableGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.TableGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.TableGrid.Location = new System.Drawing.Point(500, 65);
            this.TableGrid.Name = "TableGrid";
            this.TableGrid.ReadOnly = true;
            this.TableGrid.RowHeadersVisible = false;
            this.TableGrid.RowTemplate.Height = 28;
            this.TableGrid.Size = new System.Drawing.Size(430, 429);
            this.TableGrid.TabIndex = 1;
            // 
            // AlgCriteriaGrid
            // 
            this.AlgCriteriaGrid.AllowUserToAddRows = false;
            this.AlgCriteriaGrid.AllowUserToDeleteRows = false;
            this.AlgCriteriaGrid.AllowUserToResizeColumns = false;
            this.AlgCriteriaGrid.AllowUserToResizeRows = false;
            this.AlgCriteriaGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.AlgCriteriaGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.AlgCriteriaGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6});
            this.AlgCriteriaGrid.Location = new System.Drawing.Point(0, 35);
            this.AlgCriteriaGrid.Name = "AlgCriteriaGrid";
            this.AlgCriteriaGrid.ReadOnly = true;
            this.AlgCriteriaGrid.RowHeadersVisible = false;
            this.AlgCriteriaGrid.RowTemplate.Height = 28;
            this.AlgCriteriaGrid.Size = new System.Drawing.Size(430, 135);
            this.AlgCriteriaGrid.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "1 разряд";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 102;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "2 разряда";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 111;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "3 разряда";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 111;
            // 
            // TableCriteriaGrid
            // 
            this.TableCriteriaGrid.AllowUserToAddRows = false;
            this.TableCriteriaGrid.AllowUserToDeleteRows = false;
            this.TableCriteriaGrid.AllowUserToResizeColumns = false;
            this.TableCriteriaGrid.AllowUserToResizeRows = false;
            this.TableCriteriaGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.TableCriteriaGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.TableCriteriaGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9});
            this.TableCriteriaGrid.Location = new System.Drawing.Point(500, 35);
            this.TableCriteriaGrid.Name = "TableCriteriaGrid";
            this.TableCriteriaGrid.ReadOnly = true;
            this.TableCriteriaGrid.RowHeadersVisible = false;
            this.TableCriteriaGrid.RowTemplate.Height = 28;
            this.TableCriteriaGrid.Size = new System.Drawing.Size(430, 135);
            this.TableCriteriaGrid.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "1 разряд";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 102;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "2 разряда";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 111;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "3 разряда";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 111;
            // 
            // UserInputGrid
            // 
            this.UserInputGrid.AllowUserToAddRows = false;
            this.UserInputGrid.CausesValidation = false;
            this.UserInputGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.UserInputGrid.ColumnHeadersVisible = false;
            this.UserInputGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column4});
            this.UserInputGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.UserInputGrid.Location = new System.Drawing.Point(1016, 65);
            this.UserInputGrid.MultiSelect = false;
            this.UserInputGrid.Name = "UserInputGrid";
            this.UserInputGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.UserInputGrid.RowHeadersVisible = false;
            this.UserInputGrid.RowTemplate.Height = 28;
            this.UserInputGrid.Size = new System.Drawing.Size(240, 360);
            this.UserInputGrid.TabIndex = 4;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "";
            this.Column4.Name = "Column4";
            this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // BtnResetTables
            // 
            this.BtnResetTables.Location = new System.Drawing.Point(387, 185);
            this.BtnResetTables.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnResetTables.Name = "BtnResetTables";
            this.BtnResetTables.Size = new System.Drawing.Size(148, 35);
            this.BtnResetTables.TabIndex = 5;
            this.BtnResetTables.Text = "Пересчитать";
            this.BtnResetTables.UseVisualStyleBackColor = true;
            this.BtnResetTables.Click += new System.EventHandler(this.BtnResetTables_Click);
            // 
            // TbResult
            // 
            this.TbResult.Location = new System.Drawing.Point(1078, 35);
            this.TbResult.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TbResult.Name = "TbResult";
            this.TbResult.Size = new System.Drawing.Size(148, 26);
            this.TbResult.TabIndex = 6;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.SetUserSizeButton);
            this.panel1.Controls.Add(this.TbUserSize);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.UserInputGrid);
            this.panel1.Controls.Add(this.AlgGrid);
            this.panel1.Controls.Add(this.TableGrid);
            this.panel1.Location = new System.Drawing.Point(16, 18);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1282, 497);
            this.panel1.TabIndex = 7;
            // 
            // SetUserSizeButton
            // 
            this.SetUserSizeButton.Location = new System.Drawing.Point(1016, 432);
            this.SetUserSizeButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.SetUserSizeButton.Name = "SetUserSizeButton";
            this.SetUserSizeButton.Size = new System.Drawing.Size(150, 35);
            this.SetUserSizeButton.TabIndex = 10;
            this.SetUserSizeButton.Text = "Задать размер";
            this.SetUserSizeButton.UseVisualStyleBackColor = true;
            this.SetUserSizeButton.Click += new System.EventHandler(this.SetUserSizeButton_Click);
            // 
            // TbUserSize
            // 
            this.TbUserSize.Location = new System.Drawing.Point(1182, 432);
            this.TbUserSize.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TbUserSize.Name = "TbUserSize";
            this.TbUserSize.Size = new System.Drawing.Size(72, 26);
            this.TbUserSize.TabIndex = 9;
            this.TbUserSize.Text = "10";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1011, 42);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(181, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "Данные пользователя";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(495, 42);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(168, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "Табличные значения";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 42);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(198, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Алгоритмический метод";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, -5);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "Исходные данные";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.EvaluateUserInputButton);
            this.panel2.Controls.Add(this.AlgCriteriaGrid);
            this.panel2.Controls.Add(this.TableCriteriaGrid);
            this.panel2.Controls.Add(this.TbResult);
            this.panel2.Controls.Add(this.BtnResetTables);
            this.panel2.Location = new System.Drawing.Point(16, 545);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1282, 229);
            this.panel2.TabIndex = 8;
            // 
            // EvaluateUserInputButton
            // 
            this.EvaluateUserInputButton.Location = new System.Drawing.Point(1078, 92);
            this.EvaluateUserInputButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.EvaluateUserInputButton.Name = "EvaluateUserInputButton";
            this.EvaluateUserInputButton.Size = new System.Drawing.Size(150, 35);
            this.EvaluateUserInputButton.TabIndex = 7;
            this.EvaluateUserInputButton.Text = "Оценить";
            this.EvaluateUserInputButton.UseVisualStyleBackColor = true;
            this.EvaluateUserInputButton.Click += new System.EventHandler(this.EvaluateUserInputButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 545);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(165, 20);
            this.label2.TabIndex = 7;
            this.label2.Text = "Оценка случайности";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn1.HeaderText = "1 разряд";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 102;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "2 разряда";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 111;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "3 разряда";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 111;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1317, 792);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "MainForm";
            this.Text = "Lab01";
            ((System.ComponentModel.ISupportInitialize)(this.AlgGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TableGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AlgCriteriaGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TableCriteriaGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserInputGrid)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView AlgGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridView TableGrid;
        private System.Windows.Forms.DataGridView AlgCriteriaGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridView TableCriteriaGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridView UserInputGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.Button BtnResetTables;
        private System.Windows.Forms.TextBox TbResult;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button EvaluateUserInputButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button SetUserSizeButton;
        private System.Windows.Forms.TextBox TbUserSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
    }
}

