﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Lab01
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            UserInputGrid.Rows.Clear();
            UserInputGrid.Rows.Add(10);
            UserInputGrid.Columns.Cast<DataGridViewColumn>().ToList().ForEach(f =>
            {
                f.SortMode = DataGridViewColumnSortMode.NotSortable;
                f.MinimumWidth = 80;
            });
            AlgCriteriaGrid.Columns.Cast<DataGridViewColumn>().ToList().ForEach(f =>
            {
                f.SortMode = DataGridViewColumnSortMode.NotSortable;
                f.MinimumWidth = 80;
            });
            TableGrid.Columns.Cast<DataGridViewColumn>().ToList().ForEach(f =>
            {
                f.SortMode = DataGridViewColumnSortMode.NotSortable;
                f.MinimumWidth = 80;
            });
        }

        private void SetUserSizeButton_Click(object sender, System.EventArgs e)
        {
            try
            {
                UserInputGrid.Rows.Clear();
                UserInputGrid.Rows.Add(int.Parse(TbUserSize.Text));
            }
            catch (Exception)
            {
                MessageBox.Show("Некорректный размер");
            }
        }

        private void EvaluateUserInputButton_Click(object sender, EventArgs e)
        {
            try
            {
                var list = (UserInputGrid.Rows.Cast<object>()
                    .Select(row => int.Parse(((DataGridViewRow) row).Cells[0].EditedFormattedValue.ToString()))).ToList();
                var result = list.CheckSequence();
                TbResult.Text = result.ToString("G3");
            }
            catch (Exception exc)
            {
                MessageBox.Show("Некорректные исходные данные");
            }
        }

        private void BtnResetTables_Click(object sender, EventArgs e)
        {
            FillTable(() => new LinearRandom(), AlgGrid, AlgCriteriaGrid);

            FillTable(() => new TableRandom(), TableGrid, TableCriteriaGrid);
        }

        private void FillTable(Func<IRandom> getRandom, DataGridView table, DataGridView criteria )
        {
            var lists = new[] {new List<int>(), new List<int>(), new List<int>() };
            var rand = getRandom();
            for(int i = 0; i<1000; ++i)
                lists[0].Add(rand.Next(10));

            rand =  getRandom();
            for (int i = 0; i < 1000; ++i)
                lists[1].Add(rand.Next(100));

            rand =  getRandom();
            for (int i = 0; i < 1000; ++i)
                lists[2].Add(rand.Next(1000));

            FillGrid(table, criteria, lists);
                    
        }

        private void FillGrid(DataGridView grid, DataGridView criteriaGrid, IList<IList<int>> lists)
        {
            grid.Rows.Clear();
            for (int i = 0; i < 30; ++i)
            {
                grid.Rows.Add();

                for (int j = 0; j < 3; ++j)
                    grid.Rows[i].Cells[j].Value = lists[j][i].ToString("G3");
            }
            var criteria = lists.Select(l => l.CheckSequence()).ToList();
            criteriaGrid.Rows.Clear();
            criteriaGrid.Rows.Add();
            for (int i = 0; i < 3; ++i)
                criteriaGrid.Rows[0].Cells[i].Value = criteria[i].ToString("G3");
        }

    }
}
