﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lab01
{
    public static class RandomTester
    {
        public static double CheckSequence(this IList<int> input)
        {
            var result = 1.0;
            var deltas = new List<int>();
            for (var i = 1; i < input.Count; ++i)
                deltas.Add(input[i] - input[i - 1]);
            var interval = input.Max() - input.Min();
            result *= (input.Count / interval) < 10 ? ((double)deltas.Distinct().Count()) / (input.Count) :
                FrequencyPart(input, interval);
            result *= (deltas.Max() - deltas.Average())/deltas.Max();
            result *= EstimatePeriod(input);
            if (double.IsNaN(result))
                return 0.01;
            return result;
        }

        private static double FrequencyPart(IList<int> input, int interval)
        {
            IEnumerable<int> counts = Enumerable.Range(input.Min(), input.Max()).Select(e => input.Count(el => e == el));
            return (1 - Math.Abs(counts.Average() - ((double)input.Count/interval)) / ((double)input.Count/interval)) * 0.5;
        }

        private static double EstimatePeriod(IList<int> input)
        {
            var checkedNumbers = new List<int>();
            for (int i = 0; i < input.Count; ++i)
            {
                if (!ContainsSequence(input, i, checkedNumbers))
                    checkedNumbers.Add(input[i]);
                else
                    break;
            }
            return ((double) checkedNumbers.Count)/input.Count;
        }

        private static bool ContainsSequence(IList<int> input, int start, IList<int> sequenceToCheck)
        {
            try
            {
                if (sequenceToCheck.Count < 3)
                    return false;
                return !sequenceToCheck.Where((t, i) => t != input[i + start]).Any();
            }
            catch (ArgumentOutOfRangeException)
            {
                return true;
            }
        }
    }
}