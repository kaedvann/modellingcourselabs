﻿namespace Lab01
{
    public interface IRandom
    {
        double NextDouble();
        int Next();
        int Next(int max);
    }
}
