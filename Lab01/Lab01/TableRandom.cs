﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Lab01
{
    public class TableRandom: IRandom
    {
        private static readonly IList<int> RandomTable;
        private static readonly int Max;
        static TableRandom()
        {
            RandomTable = new List<int>((int)1e6);
            var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("Lab01.randoms.txt");
            var reader = new StreamReader(stream);
            while (!reader.EndOfStream)
            {
                var numbers = reader.ReadLine().Split(new []{' '}, StringSplitOptions.RemoveEmptyEntries);
                foreach (var number in numbers)
                {
                    RandomTable.Add(int.Parse(number));
                }
            }
            Max = RandomTable.Max();
        }

        private int _offset;

        public TableRandom() : this(new Random().Next()) { }

        public TableRandom(int seed)
        {
            _offset = seed % (RandomTable.Count-1);

        }

        public int Next()
        {
            int res = RandomTable[_offset];
            _offset++;
            if (_offset == RandomTable.Count)
                _offset = 0;
            return res;
        }

        public double NextDouble()
        {
            return ((double)Next())/Max;
        }

        public int Next(int max)
        {
            return (int) (NextDouble()*max);
        }
    }
}