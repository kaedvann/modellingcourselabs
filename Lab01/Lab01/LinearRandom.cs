﻿using System;

namespace Lab01
{
    public class LinearRandom: IRandom
    {
        private long _seed;

        private const int M = 1771875;
        private const int A = 2416;
        private const int B = 374441;

        public double NextDouble()
        {
            return ((double)Next())/M;
        }

        public LinearRandom() : this(DateTime.Now.Ticks)
        {
        }

        private LinearRandom(long seed)
        {
            _seed = seed;
        }

        public int Next()
        {
            _seed = (A*_seed + B)%M;
            return (int) _seed;
        }

        public int Next(int max)
        {
            return (int) (NextDouble()*max);
        }
    }
}