﻿using System;
using System.Collections.Generic;
using Lab01;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class RandomTesterTest
    {
        [Test]
        public void TestConstantSequence()
        {
            var input = new[] {1, 1, 1, 1, 1, 1, 1, 1};
            var result = input.CheckSequence();
            Assert.IsTrue(result <= 0.05);
        }

        [Test]
        public void TestProgression()
        {
            var input = new[] {1, 3, 5, 7, 9, 11, 13};
            var result = input.CheckSequence();
            Assert.IsTrue(result <= 0.15);
        }

        [Test]
        public void TestRandomSequence()
        {
            var input = new[] {1, 48, 3, 20, 11, 18, 100, 45, 60, 12, 400};
            var result = input.CheckSequence();
            Assert.IsTrue(result >=0.65);
        }

        [Test]
        public void TestPeriodicSequence()
        {
            var input = new[] { 1, 4, 11, 48, 17, 22, 66, 1, 4, 11, 48};
            var result = input.CheckSequence();
            Assert.IsTrue(result >= 0.35 && result <=0.8);
        }
        [Test]
        public void TestBigSequence()
        {
            var random = new Random();
            var input = new List<int>(100);
            for(int i = 0; i<1000; ++i)
                input.Add(random.Next());
            var result = input.CheckSequence();
            Assert.IsTrue(result >= 0.5);
        }

    }
}
